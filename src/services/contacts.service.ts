import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Endpoints } from '../config/Endpoints.config';
import { TokenService } from './token.service';
import { MetaService } from "./meta.service";

/**
 * Retrieves contacts for an access key retrieved in the URL
 */
@Injectable()
export class ContactService {
    public contacts: Array<Contact>;
    public selectedContact: Contact;

    constructor(
        private http: HttpClient,
        private token: TokenService,
        private meta: MetaService 
    ) {
        this.contacts = [];
    }

    public async getContactsFromAccessToken(accessToken: string) {
        let url: string = Endpoints.contacts.GET_CONTACTS_FROM_ACCESS_TOKEN_RAW.replace('{access_token}', accessToken);
        let headers = new HttpHeaders();
        headers = headers.set('X-Tenant-Id', this.token.access);
        headers = headers.set('Content-Type', 'application/json');
        let response = await this.http.get(url, {headers, observe: 'response'}).toPromise();
    
        this._mapResponseToService(response);
    }

    private _mapResponseToService(response: any) {
        if (response && response.headers) {
            if (response.headers instanceof HttpHeaders) {
                this.token.authToken = response.headers.get('X-Auth-Token');
            }
        }
        
        if (response && response.body.activityId) {
            this.meta.activityID = response.body.activityId;
            this.meta.subject = response.body.subject;

            if (response.body.contactAttendees && Array.isArray(response.body.contactAttendees)) {
                response.body.contactAttendees.map(attendee => {
                    this.contacts.push(new Contact(attendee));
                });
            }

            if (response.activityAccounts && Array.isArray(response.activityAccounts)) {
                
            }
        }
    }
}

export class Contact {
    public id: string;
    public name: string;

    constructor(raw: object) {
        this.id = raw['indskr_contactid'];
        this.name = raw['indskr_name'];
    }
}