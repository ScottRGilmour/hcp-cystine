import { Component } from '@angular/core';

/**
 * Generated class for the ThankyouComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'thankyou',
  templateUrl: 'thankyou.html'
})
export class ThankyouComponent {

  text: string;

  constructor() {
    console.log('Hello ThankyouComponent Component');
    this.text = 'Hello World';
  }

}
