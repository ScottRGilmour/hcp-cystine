import { Injectable } from '@angular/core';

@Injectable()
export class TokenService {
    public accessKey: string;
    public access: string;
    public authToken: string;

}