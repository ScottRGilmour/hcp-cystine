import { Injectable } from '@angular/core';

import Stomp from '@stomp/stompjs';
import { TokenService } from './token.service';
import { Endpoints } from '../config/Endpoints.config';
import { MetaService } from './meta.service';
import { Contact } from './contacts.service';
import { ToastController } from 'ionic-angular';
import { IFrameService } from './iframe.service';


@Injectable()
export class WebsocketDataService {
    private connected: boolean;
    private status: string;
    private client: Stomp.Client;


    constructor(
        private token: TokenService,
        private meta: MetaService,
        private toast: ToastController,
        private iframe: IFrameService
    ) {
        this.connected = false;
    }
    
    /**
     * Connects to the kafka websocket server exposing options for sending data and subscribing to topics
     * 
     * @memberof WebsocketDataService
     */
    public async connect() {
        this.status = 'Connecting to websocket';
        let url: string = Endpoints.websockets.BASE;
        this.client = Stomp.client(url);
        
        this.client.connect(this._getStompAuthorizationHeaders(), this.connectCallback.bind(this), this.connectionErrorFallback);
    }

    /**
     * Subscribes to a meeting topic and saves it so we don't override the existing
     * 
     * @param {Activity} activity 
     * @returns {Promise<boolean>} 
     * @memberof WebsocketDataService
     */
    public subscribeToMeetingTopic() {
        if (!this.connected) {
            console.log('Not connected!');
            this.connect();
            return;
        }
        
        this.client.subscribe('/topic/meetings.' + this.meta.activityID, this.handleTopicResponse.bind(this));

        // let foundSubscription = this.subscriptions.find(subscriptionActivity => subscriptionActivity.ID === activity.ID);
        // if (foundSubscription) {
        //     console.log('Already subscribed for ' + activity.ID);
        //     return;
        // }

        // this.subscriptions.push(activity);
    }

    /**
     * Internal fn for mapping activity id's for topics
     * 
     * @private
     * @param {Activity} activity 
     * @returns {string} 
     * @memberof WebsocketDataService
     */
    private _buildTopicString(): string {
        return `/topic/meetings.${this.meta.activityID}`;
    }

    /**
     * Internal fn for building mapping messages
     * 
     * @private
     * @param {Activity} activity 
     * @param {string} mapping 
     * @returns {string} 
     * @memberof WebsocketDataService
     */
    private _buildMappingString(mapping: string): string {
        return `${mapping}`;
    }

    /**
     * Internal fn for generating our security header
     * 
     * @private
     * @returns {Stomp.StompHeaders} 
     * @memberof WebsocketDataService
     */
    private _getStompAuthorizationHeaders(): Stomp.StompHeaders {
        let headers = {
            Authorization: 'Bearer ' + this.token.authToken
        }

        return headers;
    }

    public sendPractitionerJoinMeeting(contact: Contact) {

         let payload = {
            indskr_contactid: contact.id,
            indskr_name: contact.name,
            indskr_joinstatus: 100000000,
            indskr_joineddate: new Date().getTime(),
         }

         this.sendMessageToTopic(Endpoints.websockets.mappings.PRACTITIONER_JOIN_MEETING.replace('{activity_id}', this.meta.activityID), payload);
    }

    /**
     * Sends data to a topic
     * 
     * @param {Activity} activity 
     * @param {string} mapping 
     * @memberof WebsocketDataService
     */
    public sendMessageToTopic(mapping: string, payload: object) {
        this.client.send(this._buildMappingString(mapping), this._getStompAuthorizationHeaders(), JSON.stringify(payload));
    }

    /**
     * Handles responses from subscribed topics
     * 
     * @private
     * @param {Stomp.Message} response 
     * @memberof WebsocketDataService
     */
    private handleTopicResponse(response: Stomp.Message) {
        console.log(`Received response from ${response}`);

        if (!response.body) return;
        let responseObject = JSON.parse(response.body);
        let payload = JSON.parse(responseObject.payload);

        if (payload && Array.isArray(payload)) {
            let activePresentation = payload[0];
            //this.toast.create({duration: 2500, message: `${activePresentation.indskr_iopresentationname} presentation added to meeting!`}).present();
            

            if (activePresentation.activityPresentationSlides && Array.isArray(activePresentation.activityPresentationSlides)) {
                if (activePresentation.activityPresentationSlides.length > 0) {
                    this.iframe.setSource(activePresentation.activityPresentationSlides[0].indskr_slidepath);
                }
            }

            //Save presentation to service

            //Update page title with pres title
        }

    }

    /**
     * Internal fn callback for connecting
     * 
     * @private
     * @memberof WebsocketDataService
     */
    private connectCallback() {
        console.log('connected websocket');
        this.status = 'Connected to websocket';
        this.connected = true;
        this.subscribeToMeetingTopic();
    }

    /**
     * Internal fn callback for error connecting
     * 
     * @private
     * @param {any} error 
     * @memberof WebsocketDataService
     */
    private connectionErrorFallback(error) {
        console.log('error connecting to websocket', error);
    }
}