import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';
import { ContactService } from '../services/contacts.service';
import { TokenService } from '../services/token.service';
import { MetaService } from '../services/meta.service';
import { WebsocketDataService } from '../services/websocket.data.service';
import { IFrameService } from '../services/iframe.service';
import { MeetingPage } from '../pages/meeting/meeting';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    MeetingPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    MeetingPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    TokenService,
    ContactService,
    MetaService,
    WebsocketDataService,
    IFrameService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
