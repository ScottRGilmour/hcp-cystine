import { TokenService } from '../services/token.service';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';


export const Endpoints = {
    contacts: {
        GET_CONTACTS_FROM_ACCESS_TOKEN_RAW: 'https://entry.devcystine.us-east-1.indegene.io/api/rhs/accesstoken/{access_token}/meetings',
        GET_CONTACTS_FROM_ACCESS_TOKEN_HEADERS: {
            headers: new HttpHeaders(
                { 
                    'Content-Type': 'application/json',
                }
            )
        },
    },
    websockets: {
        BASE: 'wss://io-meetings-websocket-microsevice.apps.us-east-1.indegene.io:4443/echo',
        BASE_URL: 'wss://websocketmicroservice.io/meetings/{activity_id}',
        SHARE_PRESENTATION_SLIDE: 'wss://websocketmicroservice.io/meetings/{activity_id}/presentations/asset',

        mappings: {
            PRACTITIONER_JOIN_MEETING: '/app/meetings/{activity_id}/practitioners'
        }
    }
}