import { WelcomeComponent } from './../../components/welcome/welcome';
import { Component, ViewChild, Input } from '@angular/core';
import { NavController, TextInput, Button } from 'ionic-angular';
import { TokenService } from '../../services/token.service';
import { ContactService, Contact } from '../../services/contacts.service';
import { MetaService } from '../../services/meta.service';
import { WebsocketDataService } from '../../services/websocket.data.service';
import { MeetingPage } from '../meeting/meeting';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private loginStage: LoginStageEnum;
  private openInfoSection:boolean = false;
  private openPresInfo:boolean = false;
  private exitApp:boolean = false;

  constructor(
    public navCtrl: NavController, 
    private token: TokenService, 
    private contact: ContactService,
    private meta: MetaService,
    private websocket: WebsocketDataService
  ) {
    this.loginStage = LoginStageEnum.ACCESS_TOKEN;
  }

  @ViewChild('accessToken1') accessTokenInput1: TextInput;
  @ViewChild('accessToken2') accessTokenInput2: TextInput;
  @ViewChild('accessToken3') accessTokenInput3: TextInput;
  @ViewChild('accessToken4') accessTokenInput4: TextInput;
  @ViewChild('meetingJoin') meetingJoinBtn: Button;
  @ViewChild('guestNameInput') guestName: TextInput;

  ionViewDidEnter() {
    console.log('Entered application');
    this.parseURLParams();
  }
  ionViewDidLoad(){

    setTimeout(() => {
      this.accessTokenInput1.setFocus();
    },150);
  }
  openInfo()
  {
    console.log("openInfo");
    this.openPresInfo = false;
    this.exitApp = false;
    this.openInfoSection = !this.openInfoSection;
  }
  openPresenterInfo()
  {
    this.openInfoSection= false;
    this.exitApp = false;
    this.openPresInfo = !this.openPresInfo;
  }

  exit()
  {
    
    this.openPresInfo = false;
    this.openInfoSection= false;
    this.exitApp = !this.exitApp;
  }

  parseURLParams() {
    let url: string = window.location.href;
    let urlObject = new URL(url);
    let i;
    console.log(urlObject);
    this.token.access = urlObject.search.replace('?cl=', '');
    this.token.accessKey = urlObject.hash.replace('#/join-meeting/', '');
    this.accessTokenInput1.value = this.token.accessKey.charAt(0);
    this.accessTokenInput2.value = this.token.accessKey.charAt(1);
    this.accessTokenInput3.value = this.token.accessKey.charAt(2);
    this.accessTokenInput4.value = this.token.accessKey.charAt(3);
  }

  private async continueStageOne() {
    let meetingTokenAccess: string = `${this.accessTokenInput1.value}${this.accessTokenInput2.value}${this.accessTokenInput3.value}${this.accessTokenInput4.value}`;
    this.token.accessKey = meetingTokenAccess;

    await this.contact.getContactsFromAccessToken(this.token.accessKey);
    this.loginStage = LoginStageEnum.CONTACT_SELECT;
    this.websocket.connect();    
  }

  private continueStageTwo() {
    console.log('Stage two continue');
    this.websocket.sendPractitionerJoinMeeting(new Contact({indskr_name: this.guestName.value}));  
    
    this.navCtrl.push(MeetingPage);    
  }

  private clearAccessTokenInputs() {
    this.accessTokenInput1.value = '';
    this.accessTokenInput2.value = '';
    this.accessTokenInput3.value = '';
    this.accessTokenInput4.value = '';
  
    this.accessTokenInput1.setFocus();
  }

  private selectContact(contact: Contact) {
    this.contact.selectedContact = contact;
    console.log(this.contact.selectedContact);
    this.websocket.sendPractitionerJoinMeeting(contact);
    
    this.navCtrl.push(MeetingPage);
  }

  backToHome() {
    this.loginStage = LoginStageEnum.ACCESS_TOKEN;
  }

  moveFocus(nextElement) {
    nextElement.setFocus();
  }
}


export enum LoginStageEnum {
  ACCESS_TOKEN,
  CONTACT_SELECT,
  MEETING
}