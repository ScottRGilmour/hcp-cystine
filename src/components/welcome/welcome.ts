import { Component } from '@angular/core';

/**
 * Generated class for the WelcomeComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'welcome',
  templateUrl: 'welcome.html'
})
export class WelcomeComponent {

  text: string;

  constructor() {
    console.log('Hello WelcomeComponent Component');
    this.text = 'Hello World';
  }

}
