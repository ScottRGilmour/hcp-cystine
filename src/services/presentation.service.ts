import { Injectable } from "@angular/core";

@Injectable()
export class PresentationService {
    public currentPresentation: Presentation;
}

export class Presentation {
    public title: string;
}