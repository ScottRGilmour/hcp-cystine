import { NgModule } from '@angular/core';
import { WelcomeComponent } from './welcome/welcome';
import { ThankyouComponent } from './thankyou/thankyou';
@NgModule({
	declarations: [WelcomeComponent,
    ThankyouComponent],
	imports: [],
	exports: [WelcomeComponent,
    ThankyouComponent]
})
export class ComponentsModule {}
