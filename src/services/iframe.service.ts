import { Injectable, ApplicationRef } from "@angular/core";
import { DomSanitizer, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

@Injectable()
export class IFrameService {
    public sourceURL: SafeResourceUrl;
    public hasSource: boolean;

    constructor(
        private appRef: ApplicationRef,
        private domSanitizer: DomSanitizer
    ) {
        this.hasSource = false;
    }

    get iframeHeight(): number {
        return window.innerHeight;
    }

    get source(): SafeResourceUrl {
        return (this.sourceURL ? this.sourceURL : '');
    }

    setSource(url: string) {
        this.sourceURL = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
        this.appRef.tick();
        this.hasSource = true;
    }
}